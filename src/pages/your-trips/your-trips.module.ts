import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YourTripsPage } from './your-trips';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    YourTripsPage,
  ],
  imports: [
    IonicPageModule.forChild(YourTripsPage),
    SelectSearchableModule
  ],
})
export class YourTripsPageModule {}
